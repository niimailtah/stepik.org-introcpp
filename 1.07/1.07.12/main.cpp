#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n;

    cin >> n;
    vector <int> a(n + 1);

    // считывание
    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
    }
    
    // обработка
    for (int i = n; i > 0; i--)
    {
        a[i] = a[i - 1];
    }
    a[0] = a[n];

    // вывод
    for (int i = 0; i < n; i++)
    {
        cout << a[i] << " ";
    }

    return EXIT_SUCCESS;
}
