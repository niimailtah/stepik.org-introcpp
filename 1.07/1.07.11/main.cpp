#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n, temp;

    cin >> n;
    vector <int> a(n);

    // считывание
    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
    }
    
    // обработка
    for (int i = 1; i < n; i += 2)
    {
        temp = a[i];
        a[i] = a[i - 1];
        a[i - 1] = temp;
    }

    // вывод
    for (int i = 0; i < n; i++)
    {
        cout << a[i] << " ";
    }

    return EXIT_SUCCESS;
}
