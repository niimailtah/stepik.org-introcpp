#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n = 8;
    bool beat;

    vector <int> x(n), y(n);

    // считывание
    for (int i = 0; i < n; i++)
    {
        cin >> x[i] >> y[i];
    }
    
    // обработка
    for (int i = 0; i < n; i++)
    {
        beat = false;
        for (int j = i + 1; j < n; j++)
        {
            if (x[i] == x[j] ||
                y[i] == y[j] ||
                abs(x[i] - x[j]) == abs(y[i] - y[j]))
            {
                beat = true;
                break;
            }
        }
        if (beat)
        {
            break;
        }
    }

    // вывод
    if (beat)
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }

    return EXIT_SUCCESS;
}
