#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n, count;

    cin >> n;
    vector <int> a(n);

    // считывание
    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
    }
    
    // обработка
    count = 0;
    for (int i = 0; i < n; i++)
    {
        if (a[i] > 0)
        {
            count++;
        }
    }

    // вывод
    cout << count << endl;
    
    return EXIT_SUCCESS;
}
