#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n, k, l, r;

    cin >> n >> k;              // Начальные данные

    vector <bool> a(n, true);

    // считывание и обработка
    for (int i = 0; i < k; i++)
    {
        cin >> l >> r;
        for (int j = l - 1; j < r; j++)
        {
            a[j] = false;
        }
    }
    
    // вывод
    for (auto skittle : a)
    {
        if (skittle)
        {
            cout << 'I';    
        }
        else
        {
            cout << '.';
        }
    }

    return EXIT_SUCCESS;
}
