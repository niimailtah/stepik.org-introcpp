#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n, count = 0;

    cin >> n;
    vector <int> a(n);

    // считывание
    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
    }
    
    // обработка
    for (int i = 0; i < n - 1; i++)
    {
        for (int j = i + 1; j < n; j++)
        {
            if (a[i] == a[j])
            {
                count++;
            }
        }
    }

    // вывод
    cout << count << endl;

    return EXIT_SUCCESS;
}
