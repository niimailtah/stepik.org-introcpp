#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n;
    bool uniq;

    cin >> n;
    vector <int> a(n);

    // считывание
    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
    }
    
    // обработка и вывод
    for (int i = 0; i < n; i++)
    {
        uniq = true;
        for (int j = 0; j < n; j++)
        {
            if (i != j && a[i] == a[j])
            {
                uniq = false;
                break;
            }
        }
        if (uniq)
        {
            cout << a[i] << " ";
        }
    }

    return EXIT_SUCCESS;
}
