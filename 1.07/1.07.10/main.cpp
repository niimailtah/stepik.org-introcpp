#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n, count = 1;

    cin >> n;
    vector <int> a(n);

    // считывание
    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
    }
    
    // обработка
    for (int i = 1; i < n; i++)
    {
        if (a[i] > a[i - 1])
        {
            count++;
        }
    }

    // вывод
    cout << count << endl;

    return EXIT_SUCCESS;
}
