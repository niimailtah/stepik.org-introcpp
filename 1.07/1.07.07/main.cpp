#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n;

    cin >> n;
    vector <int> a(n);

    // считывание
    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
    }
    
    // обработка и вывод
    for (int i = 1; i < n; i++)
    {
        if ((a[i] > 0 && a[i - 1] > 0) ||
            (a[i] < 0 && a[i - 1] < 0))
        {
            if (a[i] > a[i - 1])
            {
                cout << a[i - 1] << " " << a[i] << endl;
            }
            else
            {
                cout << a[i] << " " << a[i - 1] << endl;
            }
            break;
        }
    }

    return EXIT_SUCCESS;
}
