#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n, min = 0;

    cin >> n;
    vector <int> a(n);

    // считывание
    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
    }
    
    // обработка
    for (int i = 0; i < n; i++)
    {
        if (a[i] > 0)
        {
            if (min == 0 ||         // первый положительный элемент
                a[i] < min)         // текущий элемент меньше минимального
            {
                min = a[i];
            }
        }
    }

    // вывод
    cout << min << endl;

    return EXIT_SUCCESS;
}
