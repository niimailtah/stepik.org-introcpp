#include <iostream>
#include <float.h>
#include <cmath>

using namespace std;

bool IsPointUpOfLine(double x, double y, double k, double b)
{
    return y >= k*x + b;
}

bool IsPointInCircle(double x, double y, double xc, double yc, double r)
{
    return (y - yc)*(y - yc) + (x - xc)*(x - xc) < r * r;
}

bool IsPointInArea(double x, double y)
{
    double k1, b1, k2, b2;
    double xc, yc, r;

    // Коэффициенты для прямых
    k1 = -1.;
    b1 = 0.;
    k2 = 2.;
    b2 = 2.;
    // Коэффициенты для окружности
    xc = 1.;
    yc = -1.;
    r = 2.;

    return (IsPointUpOfLine(x, y, k1, b1) &&
            IsPointUpOfLine(x, y, k2, b2) &&
            IsPointInCircle(x, y, xc, yc, r)) ||
            (!IsPointUpOfLine(x, y, k2, b2) &&
            !IsPointUpOfLine(x, y, k1, b1) &&
            !IsPointInCircle(x, y, xc, yc, r));
}

int main()
{
    double x, y;

    cin >> x >> y;              // Начальные данные

    // TODO: some tests had failed
    if (IsPointInArea(x, y))
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }
    

    return EXIT_SUCCESS;
}
