#include <iostream>
#include <cmath>

using namespace std;

int MinDivisor(int n)
{
    int i, result = n;

    for (i = 2; i <= sqrt(n); i++)
    {
        if (n % i == 0)
        {
            result = i;
            break;
        }
    }

    return result;
}

int main()
{
    int n;

    cin >> n;              // Начальные данные

    cout << MinDivisor(n) << endl;

    return EXIT_SUCCESS;
}
