#include <iostream>
#include <cmath>

using namespace std;

int IsPrime(int n)
{
    bool result = true;

    for (int i = 2; i <= sqrt(n); i++)
    {
        if (n % i == 0)
        {
            result = false;
            break;
        }
    }

    return result;
}

int main()
{
    int n;
    bool isPrime;

    cin >> n;              // Начальные данные

    isPrime = IsPrime(n);
    if (isPrime)
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }

    return EXIT_SUCCESS;
}
