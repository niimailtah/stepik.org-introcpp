#include <iostream>
#include <float.h>
#include <cmath>

using namespace std;

bool IsPointInSquare(double x, double y)
{
    double eps = DBL_EPSILON;
    return fabs(x) < 1. + eps && fabs(y) < 1. + eps;
}

int main()
{
    double x, y;

    cin >> x >> y;              // Начальные данные

    if (IsPointInSquare(x, y))
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }
    

    return EXIT_SUCCESS;
}
