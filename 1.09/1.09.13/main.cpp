#include <iostream>

using namespace std;

int RecursiveSum(int current_n)
{
    cin >> current_n;
    if (current_n != 0)
    {
        return current_n + RecursiveSum(current_n);
    }

    return current_n;
}

int main()
{
    cout << RecursiveSum(0) << endl;

    return EXIT_SUCCESS;
}
