#include <iostream>
#include <cmath>

using namespace std;

double distance(double x1, double y1, double x2, double y2)
{
    return sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));
}

int main()
{
    double x1, x2, y1, y2;

    cin >> x1 >> y1 >> x2 >> y2;              // Начальные данные

    cout << distance(x1, y1, x2, y2) << endl;

    return EXIT_SUCCESS;
}
