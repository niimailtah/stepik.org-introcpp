#include <iostream>
#include <float.h>
#include <cmath>

using namespace std;

bool IsPointInCircle(double x, double y, double xc, double yc, double r)
{
    double eps = DBL_EPSILON;
    return (y - yc)*(y - yc) + (x - xc)*(x - xc) <= r * r + eps;
}

int main()
{
    double x, y, xc, yc, r;

    cin >> x >> y >> xc >> yc >> r;              // Начальные данные

    if (IsPointInCircle(x, y, xc, yc, r))
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }
    

    return EXIT_SUCCESS;
}
