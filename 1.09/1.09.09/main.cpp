#include <iostream>
#include <float.h>
#include <cmath>

using namespace std;

double power(int n, double a)
{
    double result;  // to debug

    if (n == 0)
    {
        result = 1;
    }
    else if (n > 0)
    {
        result = a * power(n - 1, a);
    }
    else // n < 0
    {
        result = power(n + 1, a) / a;
    }
    
    return result;
}

int main()
{
    int n;
    double a;

    cin >> a >> n;              // Начальные данные

    cout << power(n, a) << endl;

    return EXIT_SUCCESS;
}
