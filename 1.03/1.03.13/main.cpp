#include <iostream>

using namespace std;

int main()
{
    unsigned int hours_1, minuts_1, seconds_1, hours_2, minuts_2, seconds_2, seconds;

    // Вводим исходные данные
    cin >> hours_1
        >> minuts_1
        >> seconds_1
        >> hours_2
        >> minuts_2
        >> seconds_2;

    cout << (hours_2 * 60 * 60 + minuts_2 * 60 + seconds_2) -
        (hours_1 * 60 * 60 + minuts_1 * 60 + seconds_1)
        << endl;

    return EXIT_SUCCESS;
}
