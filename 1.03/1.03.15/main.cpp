#include <iostream>

using namespace std;

int main()
{
    int number, digit_1, digit_2, digit_3, digit_4;

    cin >> number;                // Вводим исходные данные

    digit_1 = number / 1000;
    digit_2 = number / 100 % 10;
    digit_3 = number / 10 % 10;
    digit_4 = number % 10;
    cout << ((digit_1 - digit_4) * (digit_4 - digit_1)) + 
        ((digit_2 - digit_3) * (digit_3 - digit_2)) + 1
        << endl
        << (digit_1 * 10 + digit_2) - (digit_4 * 10 + digit_3) + 1
        << endl;

    return EXIT_SUCCESS;
}
