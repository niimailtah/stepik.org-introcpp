#include <iostream>

using namespace std;

int main()
{
    unsigned int A, B, N, cost;

    cin >> A >> B >> N;                // Вводим исходные данные
    cost = (A * 100 + B) * N;          // Стоимость покупки в копейках

    cout << cost / 100 << " " << cost % 100 << endl;

    return EXIT_SUCCESS;
}
