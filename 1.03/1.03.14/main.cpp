#include <iostream>

using namespace std;

int main()
{
    int V, T, point;
    const int length = 109;

    // Вводим исходные данные
    cin >> V >> T;

    point = ((V * T) % length + length) % length;
    cout << point << endl;

    return EXIT_SUCCESS;
}
