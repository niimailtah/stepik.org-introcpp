#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    unsigned int N, hours, minuts, seconds;

    cin >> N;                // Вводим исходные данные
    seconds = N % 60;
    minuts = (N / 60) % 60;
    hours = (N / (60 * 60)) % 24;

    cout << hours
        << ":"
        << setw(2)
        << setfill('0')
        << minuts
        << ":"
        << setw(2)
        << setfill('0')
        << seconds
        << endl;

    return EXIT_SUCCESS;
}
