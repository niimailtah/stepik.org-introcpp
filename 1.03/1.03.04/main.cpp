#include <iostream>

using namespace std;

int main()
{
    int N, K, nuts;

    cin >> N >> K;                // Вводим исходные данные
    nuts = K / N;                 // Посчитаем количество орехов, достаышихся каждой из белочек после дележа
    // Выводим разницу между тем, сколько орехов делили, и тем, сколько досталось всем белочкам после дележа
    cout << K - N * nuts << endl;

    return EXIT_SUCCESS;
}
