#include <iostream>

using namespace std;

int main()
{
    unsigned int N;

    cin >> N;                // Вводим исходные данные
    cout << (N % 100) / 10 << endl;

    return EXIT_SUCCESS;
}
