#include <iostream>

using namespace std;

int main()
{
    unsigned int students_1, students_2, students_3;

    cin >> students_1 >> students_2 >> students_3;          // Вводим исходные данные
    cout << (students_1 + 1) / 2 + (students_2 + 1) / 2 + (students_3 + 1) / 2 << endl;

    return EXIT_SUCCESS;
}
