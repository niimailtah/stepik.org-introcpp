#include <iostream>

using namespace std;

int main()
{
    unsigned int N;

    cin >> N;                // Вводим исходные данные
    cout << (N / 2 + 1) * 2 << endl;

    return EXIT_SUCCESS;
}
