#include <iostream>

using namespace std;

int main()
{
    unsigned int N, first_digit, second_digit, third_digit;

    cin >> N;                                           // Вводим исходные данные

    first_digit = N / 100;                        // 3-я цифра слева -> деление срезает все цифры меньше 100
    // 2-я, средняя цифра -> вычисляем в 2 этапа: 1) получаем 2-е левые цифры, 2) берем крйнюю левую цифру
    second_digit = (N % 100) / 10;
    third_digit = N % 10;                         // Крайняя справа цифра
    cout << (first_digit + second_digit + third_digit) << endl;

    return EXIT_SUCCESS;
}
