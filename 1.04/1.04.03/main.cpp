#include <iostream>

using namespace std;

int main()
{
    int number_1, number_2;

    cin >> number_1 >> number_2;                // Вводим исходные данные

    if (number_1 > number_2)
    {
        cout << number_1 << endl;
    }
    else
    {
        cout << number_2 << endl;
    }
    
    return EXIT_SUCCESS;
}
