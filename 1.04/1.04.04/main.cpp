#include <iostream>

using namespace std;

int main()
{
    int number_1, number_2;

    cin >> number_1 >> number_2;                // Вводим исходные данные

    if (number_1 > number_2)
    {
        cout << 1;
    }
    else if (number_1 < number_2)
    {
        cout << 2 << endl;
    }
    else
    {
        cout << 0 << endl;
    }
    
    return EXIT_SUCCESS;
}
