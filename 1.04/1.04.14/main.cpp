#include <iostream>

using namespace std;

int main()
{
    int N, M, X, Y, min_dist_to_long_size, min_dist_to_short_size, min_dist;

    cin >> N >> M >> X >> Y;                // Вводим исходные данные

    if (N > M)
    {
        min_dist_to_long_size = (M - X > X) ? X : M - X;
        min_dist_to_short_size = (N - Y > Y) ? Y : N - Y;
    }
    else
    {
        min_dist_to_long_size = (N - X > X) ? X : N - X;
        min_dist_to_short_size = (M - Y > Y) ? Y : M - Y;
    }
    min_dist = min_dist_to_long_size < min_dist_to_short_size ?
        min_dist_to_long_size :
        min_dist_to_short_size;

    cout << min_dist << endl;

    return EXIT_SUCCESS;
}
