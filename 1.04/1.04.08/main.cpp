#include <iostream>

using namespace std;

int main()
{
    int x_1, y_1, x_2, y_2;

    cin >> x_1 >> y_1 >> x_2 >> y_2;                // Вводим исходные данные

    if (x_1 == x_2 || y_1 == y_2)
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }
    
    return EXIT_SUCCESS;
}
