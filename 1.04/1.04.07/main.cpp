#include <iostream>

using namespace std;

int main()
{
    int number_1, number_2, number_3, coincidence;

    cin >> number_1 >> number_2 >> number_3;           // Вводим исходные данные

    // --------------------------------------------
    // | № | УСЛОВИЕ1 | УСЛОВИЕ2 | УСЛОВИЕ3 | ВЫВОД |
    // |---|----------|----------|----------|-------|
    // | 1 |   a==b   |   a==c   |   b==c   |   3   |
    // | 2 |   a==b   |   a!=c   |   b!=c   |   2   |
    // | 3 |   a!=b   |   a==c   |   b!=c   |   2   |
    // | 4 |   a!=b   |   a!=c   |   b==c   |   2   |
    // | 5 |   a!=b   |   a!=c   |   b!=c   |   0   |
    // ----------------------------------------------
    coincidence = 0;
    if ((number_1 == number_2) &&
        (number_1 == number_3) &&
        (number_2 == number_3))
    {
        coincidence = 3;
    }
    else if ((number_1 == number_2) &&
             (number_1 != number_3) &&
             (number_2 != number_3))
    {
        coincidence = 2;
    }
    else if ((number_1 != number_2) &&
             (number_1 == number_3) &&
             (number_2 != number_3))
    {
        coincidence = 2;
    }
    else if ((number_1 != number_2) &&
             (number_1 != number_3) &&
             (number_2 == number_3))
    {
        coincidence = 2;
    }

    cout << coincidence << endl;
    
    return EXIT_SUCCESS;
}
