#include <iostream>

using namespace std;

int main()
{
    int N, M, K;

    cin >> N >> M >> K;                // Вводим исходные данные

    if (N * M > K &&
        (K % N == 0 ||
        K % M == 0))
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }
    
    return EXIT_SUCCESS;
}
