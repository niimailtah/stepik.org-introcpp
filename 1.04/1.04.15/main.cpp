#include <iostream>

using namespace std;

int main()
{
    int number_1, number_2, number_3;

    cin >> number_1 >> number_2 >> number_3;           // Вводим исходные данные

    if (number_1 > number_2)             // number_1 > number_2
    {                                    //
        if (number_2 > number_3)         // number_1 > number_2 > number_3
        {                                //
            cout << number_3 << " " << number_2 << " " << number_1 << endl;
        }                                //
        else                             //               +----->----->------+
        {                                //               |                  |
            cout << number_2 << " ";     // number_1 > number_2 < number_3   |
            if (number_1 > number_3)     // number_1 > number_3 > number_2 <-+
            {                            //
                cout << number_3 << " " << number_1 << endl;
            }                            //
            else                         // number_3 > number_1 > number_2
            {
                cout << number_1 << " " << number_3 << endl;
            }
        }
    }
    else                                 // number_2 > number_1
    {                                    //
        if (number_1 > number_3)         // number_2 > number_1 > number_3
        {                                //
            cout << number_3 << " " << number_1 << " " << number_2 << endl;
        }                                //
        else                             //               +----->----->------+
        {                                //               |                  |
            cout << number_1 << " ";     // number_2 > number_1 < number_3   |
            if (number_2 > number_3)     // number_2 > number_3 > number_1 <-+
            {                            //
                cout << number_3 << " " << number_2 << endl;
            }                            //
            else                         // number_3 > number_2 > number_1
            {
                cout << number_2 << " " << number_3 << endl;
            }
        }
    }
    
    return EXIT_SUCCESS;
}
