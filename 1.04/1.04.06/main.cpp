#include <iostream>

using namespace std;

int main()
{
    int A, B, C;

    cin >> A >> B >> C;                // Вводим исходные данные

    // Сумма длин любых двух сторон треугольника должна быть больше длины третьей стороны
    if ((A + B > C) &&
        (A + C > B) &&
        (B + C > A))
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }
    
    return EXIT_SUCCESS;
}
