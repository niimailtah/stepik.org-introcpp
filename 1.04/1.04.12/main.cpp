#include <iostream>

using namespace std;

int main()
{
    int x_1, y_1, x_2, y_2, dx, dy;

    cin >> x_1 >> y_1 >> x_2 >> y_2;                // Вводим исходные данные

    dx = x_1 > x_2 ? x_1 - x_2 : x_2 - x_1;
    dy = y_1 > y_2 ? y_1 - y_2 : y_2 - y_1;
    if ((dx == 1 && dy == 2) ||
        (dx == 2 && dy == 1))
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }
    
    return EXIT_SUCCESS;
}
