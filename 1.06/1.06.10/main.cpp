#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int n, index;
    double x, coef, value;

    cin >> n >> x >> coef;         // Вводим начальные данные

    value = coef;
    index = n;
    while (index > 0)
    {
        cin >> coef;               // Вводим очередной коэффициент
        value = value * x + coef;
        index--;
    }

    cout << value << endl;

    return EXIT_SUCCESS;
}
