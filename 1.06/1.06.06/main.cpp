#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    double start, stop;
    int P, X, Y, rub, kop;

    cin >> P >> X >> Y;               // Вводим начальные данные

    start = (double) X * 100 + Y;     // Вычисляем начальное значение копеек вклада
    stop = start + start * P / 100;   // Вычисляем вклад на конец года
    rub = stop / 100;                 // Выделяем рубли из копеек
    kop = trunc(stop - rub * 100);    // Копейки с отбрасыканием десятичной части

    cout << rub << " " << kop << endl;

    return 0;
}
