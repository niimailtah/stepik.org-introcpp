#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    double start, stop;
    int P, X, Y, K, rub, kop, current_year;

    cin >> P >> X >> Y >> K;          // Вводим начальные данные

    current_year = 0;
    rub = X;
    kop = Y;
    while (current_year < K)
    {
        start = (double) rub * 100 + kop; // Вычисляем начальное значение копеек вклада на начало года
        stop = start + start * P / 100;   // Вычисляем вклад на конец года
        rub = stop / 100;                 // Выделяем рубли из копеек
        kop = trunc(stop - rub * 100);    // Копейки с отбрасыканием десятичной части
        current_year++;
    }

    cout << rub << " " << kop << endl;

    return 0;
}
