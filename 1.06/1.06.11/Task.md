Квадратное уравнение
==================

Даны действительные коэффициенты a, b, c, при этом a ≠ 0. Решите квадратное уравнение **ax<sup>2</sup> + bx + c = 0** и выведите все его корни.

---
## Формат входных данных ##

Вводятся три действительных числа.

---
## Формат выходных данных ##

Если уравнение имеет два корня, выведите два корня в порядке возрастания, если один корень — выведите одно число, если нет корней — не выводите ничего.

---
## Sample Input: ##
```
1
-1
-2
```

---
## Sample Output: ##
```
-1 2
```
