#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

int main()
{
    double a, b, c, D, eps = 10e-6;

    cin >> a >> b >> c;         // Вводим начальные данные

    D = b*b - 4*a*c;

    if (D >= 0)
    {
        if (fabs(D) < eps)
        {
            cout << -b / (2*a) << endl;
        }
        else
        {
            double x1, x2;

            x1 = (-b + sqrt(D))/(2*a);
            x2 = (-b - sqrt(D))/(2*a);
            if (x1 > x2)
            {
                cout << x2 << " " << x1 << endl;
            }
            else
            {
                cout << x1 << " " << x2 << endl;
            }
        }
    }

    return EXIT_SUCCESS;
}
