#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    double X, x;
    int int_x;

    cin >> X;
    x = X * 10;
    int_x = trunc(x);

    cout << int_x % 10 << endl;

    return 0;
}
