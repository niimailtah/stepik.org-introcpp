#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

int main()
{
    int current, sum = 0, count = 0, sum_of_square = 0;

    cin >> current;          // Вводим первое число или признак конца последовательности
    while (current != 0)     // Пока не признак конца последовательности
    {
        count++;             // Увеличиваем счетчик
        sum += current;
        sum_of_square += current * current;
        cin >> current;      // Вводим очередное число или признак конца последовательноси
    }
    cout << setprecision(11) << fixed;      // Форматируем вывод
    cout << sqrt( ((double) sum_of_square -
                    (double) sum * sum / count ) /
                    (count - 1) )
        << endl;   // Выводим среднее отклонение

    return EXIT_SUCCESS;
}
