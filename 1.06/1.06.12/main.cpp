#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

int main()
{
    double a, b, c, D, eps = 10e-6;

    cin >> a >> b >> c;         // Вводим начальные данные

    if (fabs(a) < eps)
    {
        if (fabs(b) < eps)
        {
            if (fabs(c) < eps)
            {
                cout << 3 << endl;
            }
            else
            {
                cout << 0 << endl;
            }
        }
        else
        {
            cout << 1 << " " << -c/b + 0 << endl;
        }
    }
    else
    {
        D = b*b - 4*a*c;

        if (D >= 0)
        {
            if (fabs(D) < eps)
            {
                cout << 1 << " " << -b / (2*a) + 0 << endl;
            }
            else
            {
                double x1, x2;

                x1 = (-b + sqrt(D))/(2*a);
                x2 = (-b - sqrt(D))/(2*a);
                if (x1 > x2)
                {
                    cout << 2 << " " << x2 << " " << x1 << endl;
                }
                else
                {
                    cout << 2 << " " << x1 << " " << x2 << endl;
                }
            }
        }
        else
        {
            cout << 0 << endl;
        }
    }

    return EXIT_SUCCESS;
}
