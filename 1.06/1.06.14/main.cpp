#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

int main()
{
    double a, b, c, d, e, f, det, det_x, det_y, x, y, n, k, eps = 10e-6;

    cin >> a >> b >> c >> d >> e >> f;         // Вводим начальные данные

    det = a*d - b*c;
    det_x = e*d - f*b;
    det_y = a*f - c*e;

    if (fabs(det) < eps)
    {
        if (fabs(det_x) < eps && fabs(det_y) < eps)
        {
            if (fabs(a) < eps &&
                fabs(b) < eps &&
                fabs(c) < eps &&
                fabs(d) < eps)
            {
                if (fabs(e) > eps ||
                    fabs(f) > eps)
                {
                    cout << 0 << endl;
                }
                else
                {
                    cout << 5 << endl;
                }
            }
            else
            {
                if (fabs(a) < eps &&
                    fabs(c) < eps)
                {
                    if (fabs(b) > eps)
                    {
                        y = e / b;
                    }
                    else
                    {
                        y = f / d;
                    }
                    cout << 4 << " " << y  << endl;
                }
                else
                {
                    if (fabs(b) < eps &&
                        fabs(d) < eps)
                    {
                        if (fabs(a) > eps)
                        {
                            x = e / a;
                        }
                        else
                        {
                            x = f /  c;
                        }
                        cout << 3 << " " << x << endl;
                    }
                    else
                    {
                        if (fabs(b) > eps)
                        {
                            n = e / b;
                            k = -a / b;
                        }
                        else
                        {
                            n = f / d;
                            k = -c / d;
                        }
                        cout << 1 << " " << k << " " << n << endl;
                    }
                }
            }
        }
        else
        {
            cout << 0 << endl;
        }
    }
    else
    {
        x = det_x / det;
        y = det_y / det;

        cout << 2 << " " << x << " " << y << endl;
    }

    return EXIT_SUCCESS;
}
