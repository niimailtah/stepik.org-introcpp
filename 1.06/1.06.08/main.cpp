#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int current, sum = 0, count = 0;

    cin >> current;          // Вводим первое число или признак конца последовательности
    while (current != 0)     // Пока не признак конца последовательности
    {
        count++;             // Увеличиваем счетчик
        sum += current;
        cin >> current;      // Вводим очередное число или признак конца последовательноси
    }
    cout << setprecision(11) << fixed;      // Форматируем вывод
    cout << (double) sum / count << endl;   // Выводим среднее значение

    return 0;
}
