#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

int main()
{
    double a, b, c, d, e, f, det, det_x, det_y, x, y, eps = 10e-6;

    cin >> a >> b >> c >> d >> e >> f;         // Вводим начальные данные

    det = a*d - b*c;
    det_x = e*d - f*b;
    det_y = a*f - c*e;

    x = det_x / det;
    y = det_y / det;

    cout << x << " " << y << endl;

    return EXIT_SUCCESS;
}
