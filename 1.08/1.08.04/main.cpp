#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n, i, j;

    cin >> n;              // Начальные данные

    vector<vector<char>> a(n, vector<char>(n, '.'));

    // обработка
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            if (i == n / 2 ||       // горизонталь
                j == n / 2 ||       // вертикаль
                i == j ||           // главная диагональ
                i + j == n - 1)     // обртаная диагональ
            {
                a[i][j] = '*';
            }
        }
    }
    
    // вывод
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            cout << a[i][j] << " ";
        }
        cout << endl;
    }

    return EXIT_SUCCESS;
}
