#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n, m, i, j, ii, jj, temp;

    cin >> n >> m;              // Начальные данные

    vector<vector<int>> a(n, vector<int>(m));

    // ввод
    for (ii = 0; ii < n; ii++)
    {
        for (jj = 0; jj < m; jj++)
        {
            cin >> a[ii][jj];
        }
    }
    cin >> i >> j;

    // обработка
    for (ii = 0; ii < n; ii++)
    {
        temp = a[ii][i];
        a[ii][i] = a[ii][j];
        a[ii][j] = temp;
    }

    // вывод
    for (ii = 0; ii < n; ii++)
    {
        for (jj = 0; jj < m; jj++)
        {
            cout << a[ii][jj] << " ";
        }
        cout << endl;
    }

    return EXIT_SUCCESS;
}
