#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n, m, i, j, max, max_i, max_j;

    cin >> n >> m;              // Начальные данные

    vector<vector<int>> a(n, vector<int>(m));

    // считывание
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < m; j++)
        {
            cin >> a[i][j];
        }
    }

    // обработка
    max_i = 0;
    max_j = 0;
    max = a[max_i][max_j];
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < m; j++)
        {
            if (a[i][j] > max)
            {
                max_i = i;
                max_j =j;
                max = a[max_i][max_j];
            }
        }
    }
    
    // вывод
    cout << max_i << " " << max_j << endl;

    return EXIT_SUCCESS;
}
