#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n, i, j;
    bool simmetrical;

    cin >> n;              // Начальные данные

    vector<vector<int>> a(n, vector<int>(n));

    // ввод
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            cin >> a[i][j];
        }
    }

    // обработка
    simmetrical = true;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            if (i != j &&
                a[i][j] != a[j][i])
            {
                simmetrical = false;
                break;
            }
        }
        if (!simmetrical)
        {
            break;
        }
    }

    // вывод
    if (simmetrical)
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }

    return EXIT_SUCCESS;
}
