#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n, k, i, j, ii, jj;

    cin >> n;              // Начальные данные

    vector<vector<int>> a(n, vector<int>(n));

    // ввод
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            cin >> a[i][j];
        }
    }
    cin >> k;

    // обработка и вывод
    ii = k < 0 ? 0 : k;
    jj = k < 0 ? -k : 0;
    for (i = ii, j = jj; i < n && j < n; i++, j++)
    {
        cout << a[i][j] << " ";
    }

    return EXIT_SUCCESS;
}
