#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;

int main()
{
    int n, m, i, j, k, current = 1;

    cin >> n >> m;              // Начальные данные

    vector<vector<int>> a(n, vector<int>(m, 0));

    // ввод и обработка
    for (k = 0; k < m + n - 1; k++)   // цикл по диагоналям
    {
        for (i = 0; i < n; i++)       // Стандартные циклы
        {                             // по строкам
            for (j = 0; j < m; j++)   // и столбцам
            {
                if (i == k - j ||
                    j == k - i)
                {
                    a[i][j] = current;
                    current++;
                }
            }
        }
    }

    // вывод
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < m; j++)
        {
            cout << setw(4) << a[i][j];
        }
        cout << endl;
    }

    return EXIT_SUCCESS;
}
