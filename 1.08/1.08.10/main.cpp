#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n, m, k, i, j, count;
    bool found = false;

    cin >> n >> m;              // Начальные данные

    vector<vector<int>> a(n, vector<int>(m));

    // ввод
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < m; j++)
        {
            cin >> a[i][j];
        }
    }
    cin >> k;

    // обработка
    for (i = 0; i < n; i++)
    {
        count = 0;
        for (j = 0; j < m; j++)
        {
            if (a[i][j] == 0)
            {
                count++;
                if (count == k)
                {
                    found = true;
                    break;
                }
            }
            else
            {
                count = 0;
            }
        }
        if (found)
        {
            break;
        }
    }

    // вывод
    if (found)
    {
        cout << i + 1 << endl;
    }
    else
    {
        cout << 0 << endl;
    }

    return EXIT_SUCCESS;
}
