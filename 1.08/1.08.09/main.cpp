#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n, m, i, j;

    cin >> n >> m;              // Начальные данные

    vector<vector<int>> a(n, vector<int>(m));
    vector<vector<int>> b(m, vector<int>(n));

    // ввод
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < m; j++)
        {
            cin >> a[i][j];
        }
    }

    // обработка
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < m; j++)
        {
            b[j][i] = a[i][j];
        }
    }

    // вывод
    for (i = 0; i < m; i++)
    {
        for (j = 0; j < n; j++)
        {
            cout << b[i][j] << " ";
        }
        cout << endl;
    }

    return EXIT_SUCCESS;
}
