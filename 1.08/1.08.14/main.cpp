#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;

int main()
{
    int n, m, i, j, current = 1;
    bool trigger = true;

    cin >> n >> m;              // Начальные данные

    vector<vector<int>> a(n, vector<int>(m, 0)); // Инициалицация 0-ми

    // ввод и обработка
    for (i = 0; i < n; i++)
    {
        trigger = (i % 2 == 0);            // Выводить ли очередное значение?
        for (j = 0; j < m; j++)
        {
            if (trigger)
            {
                a[i][j] = current;
                current++;
            }
            trigger = !trigger;
        }
    }

    // вывод
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < m; j++)
        {
            cout << setw(4) << a[i][j];
        }
        cout << endl;
    }

    return EXIT_SUCCESS;
}
