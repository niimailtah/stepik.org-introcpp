#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n, i, j;

    cin >> n;              // Начальные данные

    vector<vector<int>> a(n, vector<int>(n));

    // обработка
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            a[i][j] = abs(i - j);
        }
    }
    
    // вывод
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            cout << a[i][j] << " ";
        }
        cout << endl;
    }

    return EXIT_SUCCESS;
}
