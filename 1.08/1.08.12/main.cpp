#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;

int main()
{
    int n, m, i, j, current = 1;

    cin >> n >> m;              // Начальные данные

    vector<vector<int>> a(n, vector<int>(m));

    // ввод и обработка
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < m; j++)
        {
            if (i % 2 == 0)
            {
                a[i][j] = current;
            }
            else
            {
                a[i][m - j - 1] = current;
            }
            current++;
        }
    }

    // вывод
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < m; j++)
        {
            cout << setw(4) << a[i][j];
        }
        cout << endl;
    }

    return EXIT_SUCCESS;
}
