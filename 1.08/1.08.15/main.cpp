#include <iostream>
#include <iomanip>
#include <limits.h>
#include <vector>

using namespace std;

int main()
{
    int n, m, i, j, di, dj, current_dir, current = 1, border = INT_MAX;

    cin >> n >> m;              // Начальные данные

    vector<vector<int>> a(n + 2, vector<int>(m + 2, 0)); // Инициалицация 0-ми
    vector<vector<int>> dir(4, vector<int>(2));          // Направления

    // Устанавливаем границы
    for (i = 0; i < n + 2; i++)
    {
        a[i][0] = border;
        a[i][m + 1] = border;
    }
    for (j = 0; j < m + 2; j++)
    {
        a[0][j] = border;
        a[n + 1][j] = border;
    }
    // Устанавливаем направления соответственно карте в северном полушарии
    const int N = 0,    // Nord
        E = 1,    // East
        S = 2,    // South
        W = 3,    // West
        DI = 0,   // Индекс в массиве направлений по строкам
        DJ = 1;   // Индекс в массиве направлений по колонкам
                    dir[N][DI] = -1;
                    dir[N][DJ] =  0;
    dir[W][DI] =  0;                dir[E][DI] = 0;
    dir[W][DJ] = -1;                dir[E][DJ] = 1;
                    dir[S][DI] =  1;
                    dir[S][DJ] =  0;

    // ввод и обработка
    i = 1;                 // Начинаем с левого
    j = 0;                 // и верхнего угла
    current_dir = E;       // Устанавливаем направление на восток
    di = dir[current_dir][DI];
    dj = dir[current_dir][DJ];

    while (a[i + di][j + dj] == 0 ||     // Пока есть неинициализированные значения
        a[i + di][j + dj] != border)     // Пока не уткнулись в ограду
    {
        // Некуда бежать
        if (a[i + di][j + dj] != border &&
            a[i + di][j + dj] != 0)
        {
            break;
        }
        a[i + di][j + dj] = current;
        current++;
        i += di;
        j += dj;
        // Поворачиваем направо по движению
        if (a[i + di][j + dj] != 0 ||
            a[i + di][j + dj] == border)
        {
            switch (current_dir)
            {
                case E:
                    current_dir = S;
                    break;
                case S:
                    current_dir = W;
                    break;
                case W:
                    current_dir = N;
                    break;
                case N:
                    current_dir = E;
                    break;
                default:
                    cout << "Anybody gueses to realise this case" << endl;
                    break;
            }
            di = dir[current_dir][DI];
            dj = dir[current_dir][DJ];
        }
    }

    // вывод
    for (i = 1; i < n + 1; i++)
    {
        for (j = 1; j < m + 1; j++)
        {
            cout << setw(4) << a[i][j];
        }
        cout << endl;
    }

    return EXIT_SUCCESS;
}
