#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

using namespace std;


bool comp(const pair <int, int>& x, const pair <int, int>& y)
{
    if (x.second == y.second)
    {
        return x.first < y.first;
    }
    return x.second > y.second;
}


int main()
{
    int n;

    // ввод
    cin >> n;
    vector <pair <int, int>> a(n);
    for (int i = 0; i < n; i++)
    {
        int id, grade;

        cin >> id >> grade;
        a[i] = {id, grade}; // создание пары id - балл
    }

    // обработка
    sort(a.begin(), a.end(), comp);

    // вывод
    for (auto now : a)
    {
        cout << now.first << " " << now.second << endl;
    }

    return EXIT_SUCCESS;
}
