#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;


int main()
{
    int n;

    // ввод
    cin >> n;
    vector <int> a(n);
    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
    }

    // обработка
    sort(a.begin(), a.end());

    // вывод
    for (auto now : a)
    {
        cout << now << " ";
    }
    cout << endl;

    return EXIT_SUCCESS;
}
