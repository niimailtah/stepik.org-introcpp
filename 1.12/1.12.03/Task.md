Сортировака массива
=================

Отсортируйте массив.

---
## Формат входных данных ##

Первая строка входных данных содержит количество элементов в массиве N ≤ $10^5$. Далее идет N целых чисел, не превосходящих по абсолютной величине $10^9$.

---
## Формат выходных данных ##

Выведите эти числа в порядке неубывания.

---
## Sample Input: ##
```
5
5 4 3 2 1
```

---
## Sample Output: ##
```
1 2 3 4 5
```
