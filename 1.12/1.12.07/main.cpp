#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <cmath>
#include <string>

using namespace std;
typedef pair<pair<string, string>, vector<int> > TGrades;


bool comp(const TGrades & a, const TGrades & b)
{
    return ((double(a.second[0]) +
            double(a.second[1]) +
            double(a.second[2])) / 3) >
            ((double(b.second[0]) +
            double(b.second[1]) +
            double(b.second[2])) / 3);
}


int main()
{
    int n;

    // ввод
    cin >> n;
    vector <TGrades> a(n);
    for (int i = 0; i < n; i++)
    {
        string name, surname;
        int math, phis, info;

        cin >> name >> surname >> math >> phis>> info;
        a[i] = {{name, surname}, {math, phis, info}};
    }

    // обработка
    stable_sort(a.begin(), a.end(), comp);

    // вывод
    for (auto now : a)
    {
        cout << now.first.first << " " << now.first.second << endl;
    }

    return EXIT_SUCCESS;
}
