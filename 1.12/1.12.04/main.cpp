#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;


int main()
{
    int size, current_size, n, c;
    bool first_pair;

    // ввод
    cin >> size >> n;
    vector <int> a(n);
    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
    }

    // обработка
    sort(a.begin(), a.end());
    current_size = a[0];
    c = 0;
    first_pair = true;
    for (auto now : a)
    {
        if (first_pair)
        {
            if (now >= size)
            {
                first_pair = false;
                c++;
                current_size = now;
            }
        }
        else
        {
            if (now >= current_size + 3)
            {
                c++;
                current_size = now;
            }
        }
    }

    // вывод
    cout << c << endl;

    return EXIT_SUCCESS;
}
