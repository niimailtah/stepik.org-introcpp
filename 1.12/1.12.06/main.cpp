#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <cmath>

using namespace std;


bool comp(const pair <int, int>& a, const pair <int, int>& b)
{
    return hypot(a.first, a.second) < hypot(b.first, b.second);
}


int main()
{
    int n;

    // ввод
    cin >> n;
    vector <pair <int, int>> a(n);
    for (int i = 0; i < n; i++)
    {
        int x, y;

        cin >> x >> y;
        a[i] = {x, y};
    }

    // обработка
    sort(a.begin(), a.end(), comp);

    // вывод
    for (auto now : a)
    {
        cout << now.first << " " << now.second << endl;
    }

    return EXIT_SUCCESS;
}
