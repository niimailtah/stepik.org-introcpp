#include <iostream>

using namespace std;

int main()
{
    int number, pow2 = 1;

    cin >> number;                            // Вводим исходные данные

    while (pow2 < number)
    {
        pow2 *= 2;
    }
    cout << (pow2 == number ? "YES" : "NO") << endl;

    return EXIT_SUCCESS;
}
