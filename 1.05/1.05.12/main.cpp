#include <iostream>

using namespace std;

int main()
{
    int N, i, current, pre_N, pre_pre_N;

    cin >> N;                // Ввод первоначальных данных
    
    current = (N == 0 ? 0 : 1);
    if (N > 1)
    {
        i = 2;
        pre_pre_N = 0;
        pre_N = 1;
        while (i <= N)
        {
            current = pre_N + pre_pre_N;
            pre_pre_N = pre_N;
            pre_N = current;
            i++;
        }
    }
    cout << current << endl;

    return EXIT_SUCCESS;
}
