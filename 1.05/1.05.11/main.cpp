#include <iostream>

using namespace std;

int main()
{
    int current, max, pre_max;

    cin >> current;          // Вводим первое число
    max = current;
    cin >> current;          // Вводим второе число
    pre_max = current < max ? current : 0;

    while (current != 0)     // Пока не признак конца последовательности
    {
        if (current >= max)
        {
            pre_max = max;   // Сохраняем значение предыдущего максимума
            max = current;
        }
        else
        {
            if (current > pre_max)
            {
                pre_max = current;
            }
            
        }
        cin >> current;      // Вводим очередное число или признак конца последовательноси
    }
    cout << pre_max << endl; // Выводим второй по величине элемент

    return EXIT_SUCCESS;
}
