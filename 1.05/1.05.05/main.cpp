#include <iostream>

using namespace std;

int main()
{
    int N, pow2 = 1;

    cin >> N;                            // Вводим исходные данные

    while (pow2 <= N)
    {
        cout << pow2 << " ";
        pow2 *= 2;
    }
    cout << endl;

    return EXIT_SUCCESS;
}
