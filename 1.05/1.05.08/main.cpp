#include <iostream>

using namespace std;

int main()
{
    int current, sum = 0;

    cin >> current;          // Вводим первое число или признак конца последовательности
    while (current != 0)     // Пока не признак конца последовательности
    {
        sum += current;      // Вычисляем текущую сумму
        cin >> current;      // Вводим очередное число или признак конца последовательноси
    }
    cout << sum << endl;     // Выводим сумму введенных чисел

    return EXIT_SUCCESS;
}
