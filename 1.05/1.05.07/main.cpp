#include <iostream>

using namespace std;

int main()
{
    int current, count = 0;

    cin >> current;          // Вводим первое число или признак конца последовательности
    while (current != 0)     // Пока не признак конца последовательности
    {
        count++;             // Увеличиваем счетчик
        cin >> current;      // Вводим очередное число или признак конца последовательноси
    }
    cout << count << endl;   // Выводим количество введенных чисел

    return EXIT_SUCCESS;
}
