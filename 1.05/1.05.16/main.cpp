#include <iostream>

using namespace std;

int main()
{
    int current, prev, index_prev_local_max;
    bool up = false;
    bool first = true;
    bool first_local_max = true;
    int min_dist = 0;
    int dist = 0;
    int index = 0;

    cin >> current;          // Вводим первое число

    while (current != 0)     // Пока не признак конца последовательности
    {
        prev = current;      // Сохраняем предыдущее число последовательности
        cin >> current;      // Вводим очередное число или признак конца последовательноси
        if (current == 0)    // Поймали признак конца последовательности
        {
            break;
        }
        if (current > prev)
        {
            up = true;
        }
        else if (current == prev)
        {
            up = false;
        }
        else
        {                      // Если
            if (up &&          // был подъем И
                !first)        // не начальный элемент последовательности
            {
                if (first_local_max)
                {
                    first_local_max = false;
                }
                else
                {
                    dist = index - index_prev_local_max;
                    if (min_dist == 0 ||   // первый и пока единственный промежуток
                        dist < min_dist)   // выбираем минимальный промежуток
                    {
                        min_dist = dist;
                    }
                }
                index_prev_local_max = index;
            }
            up = false;
        }
        first = false;
        index++;
    }

    cout << min_dist << endl;

    return EXIT_SUCCESS;
}
