#include <iostream>

using namespace std;

int main()
{
    int number, current = 2;

    cin >> number;                            // Вводим исходные данные

    while (number % current != 0)
    {
        current++;
    }
    cout << current << endl;

    return EXIT_SUCCESS;
}
