#include <iostream>

using namespace std;

int main()
{
    int A, n, current, pre_N, pre_pre_N;

    cin >> A;                // Ввод первоначальных данных
    
    pre_pre_N = 0;
    pre_N = 1;
    current = pre_N + pre_pre_N;
    n = 1;
    while (current != A)
    {
        current = pre_N + pre_pre_N;
        if (current > A)
        {
            n = -1;
            break;
        }
        pre_pre_N = pre_N;
        pre_N = current;
        n++;
    }
    cout << n << endl;

    return EXIT_SUCCESS;
}
