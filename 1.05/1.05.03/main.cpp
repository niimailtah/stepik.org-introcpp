#include <iostream>

using namespace std;

int main()
{
    int N, square, current = 1;

    cin >> N;                            // Вводим исходные данные

    square = current * current;
    while (square <= N)
    {
        cout << square << " ";
        current = current + 1;
        square = current * current;
    }
    cout << endl;

    return EXIT_SUCCESS;
}
