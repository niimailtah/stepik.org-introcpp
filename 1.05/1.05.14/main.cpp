#include <iostream>

using namespace std;

int main()
{
    int current, prev, count, len;

    cin >> current;          // Вводим первое число
    count = 1;
    len = 1;

    while (current != 0)     // Пока не признак конца последовательности
    {
        prev = current;
        cin >> current;      // Вводим очередное число или признак конца последовательноси
        if (current == prev)
        {
            count++;
            if (count > len)
            {
                len = count;
            }
        }
        else
        {
            count = 1;
        }
    }

    cout << len << endl;

    return EXIT_SUCCESS;
}
