#include <iostream>

using namespace std;

int main()
{
    int current, prev;
    bool up = false;
    bool first = true;
    int count_of_local_max = 0;

    cin >> current;          // Вводим первое число

    while (current != 0)     // Пока не признак конца последовательности
    {
        prev = current;      // Сохраняем предыдущее число последовательности
        cin >> current;      // Вводим очередное число или признак конца последовательноси
        if (current == 0)    // Поймали признак конца последовательности
        {
            break;
        }
        if (current > prev)
        {
            up = true;
        }
        else if (current == prev)
        {
            up = false;
        }
        else
        {                      // Если
            if (up &&          // был подъем И
                !first)        // не начальный элемент последовательности
            {
                count_of_local_max++;
            }
            up = false;
        }
        first = false;
    }

    cout << count_of_local_max << endl;

    return EXIT_SUCCESS;
}
