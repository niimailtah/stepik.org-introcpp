#include <iostream>
#include <set>

using namespace std;


int main()
{
    set <int> s;
    int n, x, i;
    cin >> n;

    for (i = 0; i < n; i++)
    {
        cin >> x;
        s.insert(x);
    }

    cout << s.size() << endl;

    return EXIT_SUCCESS;
}
