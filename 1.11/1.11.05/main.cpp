#include <iostream>
#include <set>

using namespace std;


int main()
{
    set <int> s;
    int n, m, x, i, equals;

    // ввод
    cin >> n;
    for (i = 0; i < n; i++)
    {
        cin >> x;
        s.insert(x);
    }

    // ввод и обработка
    equals = 0;
    cin >> m;
    for (i = 0; i < m; i++)
    {
        cin >> x;
        if (s.find(x) != s.end())
        {
            equals++;
        }
    }

    // вывод
    cout << equals << endl;

    return EXIT_SUCCESS;
}
