#include <iostream>
#include <set>

using namespace std;


int main()
{
    set <int> s;
    set <int> result;
    int n, m, x, i;

    // ввод
    cin >> n;
    for (i = 0; i < n; i++)
    {
        cin >> x;
        s.insert(x);
    }

    // ввод и обработка
    cin >> m;
    for (i = 0; i < m; i++)
    {
        cin >> x;
        if (s.find(x) != s.end())
        {
            result.insert(x);
        }
    }

    // вывод
    for (auto element: result)
    {
        cout << element << " ";
    }
    cout << endl;

    return EXIT_SUCCESS;
}
