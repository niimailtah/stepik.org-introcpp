#include <iostream>
#include <string>
#include <map>

using namespace std;


int main()
{
    map <string, string> d;
    string key, value, x, result;
    int n, i;

    // ввод
    cin >> n;
    for (i = 0; i < n; i++)
    {
        cin >> key >> value;
        d[key] = value;
    }
    cin >> x;

    // обработка
    for (auto element: d)
    {
        if (x == element.first)
        {
            result = element.second;
        }
        else if (x == element.second)
        {
            result = element.first;
        }
    }

    // вывод
    cout << result << endl;

    return EXIT_SUCCESS;
}
