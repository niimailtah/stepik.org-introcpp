#include <iostream>
#include <set>

using namespace std;


int main()
{
    set <int> s;
    int n, x, i;
    cin >> n;

    for (i = 0; i < n; i++)
    {
        cin >> x;
        cout << ((s.find(x) == s.end()) ? "NO" : "YES") << endl;
        s.insert(x);
    }

    return EXIT_SUCCESS;
}
