#include <iostream>
#include <string>

using namespace std;


int main()
{
    string s;
    char c, C;

    getline(cin, s);
    c = s[0];
    if (c >= 'a' && c <= 'z')
    {
        C = 'A' + (c - 'a');
        cout << C << endl;
    }
    else
    {
        cout << c << endl;
    }

    return EXIT_SUCCESS;
}
