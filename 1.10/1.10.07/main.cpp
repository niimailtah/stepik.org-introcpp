#include <iostream>
#include <string>

using namespace std;


int main()
{
    string s;
    bool is_polindrom = true;

    getline(cin, s);
    for (size_t i = 0; i < s.size() / 2; i++)
    {
        if (s[i] != s[s.size() - i - 1])
        {
            is_polindrom = false;
            break;
        }
    }

    if (is_polindrom)
    {
        cout << "yes" << endl;
    }
    else
    {
        cout << "no" << endl;
    }
    

    return EXIT_SUCCESS;
}
