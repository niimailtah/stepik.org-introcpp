#include <iostream>
#include <string>

using namespace std;


int main()
{
    string source, max_length_string;
    size_t max_length = 0, current_length;
    size_t begin_pos = 0, end_pos = 0;

    getline(cin, source);
    source.append(" ");
    while ((end_pos = source.find(' ', begin_pos)) != string::npos)
    {
        current_length = end_pos - begin_pos;
        if (current_length > max_length)
        {
            max_length = current_length;
            max_length_string = source.substr(begin_pos, current_length);
        }
        begin_pos = source.find_first_not_of(' ', end_pos);
    }

    cout << max_length_string << endl;

    return EXIT_SUCCESS;
}
