#include <iostream>
#include <string>

using namespace std;


int main()
{
    string s;
    int word_count = 1;
    size_t pos = 0;

    getline(cin, s);
    while ((pos = s.find(' ', pos)) != string::npos)
    {
        word_count++;
        pos = s.find_first_not_of(' ', pos);
    }

    cout << word_count << endl;

    return EXIT_SUCCESS;
}
