#include <iostream>
#include <string>

using namespace std;

bool is_correct_section(const string &content)
{
    bool result = true;

    size_t  num_digits;
    int num;  

    try
    {
        num = stoi(content, &num_digits);
        
        // вся ли строка преобразовалась в число?
        if (num_digits != content.length())
        {
            throw invalid_argument(content);
        }
        // проверка на лидирующие 0
        if (content[0] == '0' && content.length() > 1)
        {
            throw invalid_argument(content);
        }
        // лежит ли число в заданном диапазоне?
        if (num < 0 || num > 255)
        {
            throw out_of_range(content);
        }
    } // ловим все исключения в одном блоке для краткости
    catch(...)
    {
        result = false;
    }

    return result;
}

int main()
{
    string source, section;
    bool is_ip_address = true;
    size_t begin_section = 0, end_section = 0;

    getline(cin, source);

    // 1 section
    end_section = source.find(".", begin_section);
    if (end_section == string::npos)
    {
        is_ip_address = false;
    }
    else
    {
        section = source.substr(begin_section, end_section - begin_section);
        is_ip_address = is_correct_section(section);
        if (is_ip_address)
        {
            // 2 section
            begin_section = end_section + 1;
            end_section = source.find(".", begin_section);
            if (end_section == string::npos)
            {
                is_ip_address = false;
            }
            else
            {
                section = source.substr(begin_section, end_section - begin_section);
                is_ip_address = is_correct_section(section);
                if (is_ip_address)
                {
                    // 3 section
                    begin_section = end_section + 1;
                    end_section = source.find(".", begin_section);
                    if (end_section == string::npos)
                    {
                        is_ip_address = false;
                    }
                    else
                    {
                        section = source.substr(begin_section, end_section - begin_section);
                        is_ip_address = is_correct_section(section);
                        if (is_ip_address)
                        {
                            // 4 section
                            begin_section = end_section + 1;
                            section = source.substr(begin_section);
                            is_ip_address = is_correct_section(section);
                        }
                    }
                }
            }
        }
    }

    if (is_ip_address)
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }
    

    return EXIT_SUCCESS;
}
